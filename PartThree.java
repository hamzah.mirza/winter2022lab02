import java.util.Scanner;
public class PartThree{
	public static void main(String[]args){
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter the length of the side of the square");
		double side = reader.nextDouble();
		double areaSquare = AreaComputations.areaSquare(side);
		System.out.println("The area of the square is " +areaSquare);
		
		System.out.println("Enter the length and width of the rectangle");
		double length = reader.nextDouble();
		double width = reader.nextDouble();
		AreaComputations ac = new AreaComputations();
		double areaRectangle = ac.areaRectangle(length, width);
		System.out.println("The area of the rectangle is " +areaRectangle);
	}
}

