public class MethodsTest {
	public static void main (String[]args){
		int x = 10;
		System.out.println (x);
		methodNoInputNoReturn();
		System.out.println(x);

		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		
		methodTwoInputNoReturn (22, 7.234);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double sRoot = sumSquareRoot(6, 3);
		System.out.println(sRoot);
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		int a = SecondClass.addOne(50);
		System.out.println(a);
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x=50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int y){
		System.out.println("Inside the method one input no return");
		System.out.println(y);
	}
	
	public static void methodTwoInputNoReturn(int x, double y){
		System.out.println("I assume that you want us to print out the values passed into methodTwoInputNoReturn");
		System.out.println(x+ "," +y);
	}
	
	public static int methodNoInputReturnInt(){
		return 6;
	}
	
	public static double sumSquareRoot(int x, int y){
		double z = x + y;
		return Math.sqrt(z);
	}
}

